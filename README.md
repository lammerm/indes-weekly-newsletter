# INDES Weekly Newsletter

Die INDES Graz Wochenmail 2.0


## Benötigte Werkzeuge

* Editor (z.B. Sublime -> https://www.sublimetext.com/3 )
* Browser (Chrome funktioniert zuverlässig)


## Ablauf

* weekly.html bearbeiten
* Datei in Browser öffnen
* Im Browser alles markieren und kopieren
* In Webclient einfügen und verschicken
* Glücklich sein!